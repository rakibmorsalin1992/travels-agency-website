import React, { Fragment, useEffect, useState } from "react";
import Navbar2 from "../../components/Navbar2";
import PageTitle from "../../components/pagetitle";
import Footer from "../../components/footer";
import Scrollbar from "../../components/scrollbar";
import Logo from "../../images/logo.png";
import Service5 from "../../components/Service5";
import { GET_MENUS_ALL_NESTED } from "../../constant/constants";

const ServicePage = () => {
  const [homeId, setHomeId] = useState();

  useEffect(() => {
    fetch(`${GET_MENUS_ALL_NESTED}`)
      .then((response) => response.json())
      .then((data) => {
        data.menus.find((e) => (e.name === "Home" ? setHomeId(e.id) : null));
      })
      .catch(() => {});
  }, []);
  console.log("nnnnnnn", homeId);
  return (
    <Fragment>
      <Navbar2 id={homeId} Logo={Logo} />
      <PageTitle pageTitle={"Service"} pagesub={"Service"} />
      <Service5 />
      <Footer />
      <Scrollbar />
    </Fragment>
  );
};
export default ServicePage;
