import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { GET_CONTENTS_BY_MENU_ID } from "../../constant/constants";
// const parse = require("html-react-parser");
import { Interweave } from "interweave";

function items(obj) {
  let content = [];

  // eslint-disable-next-line no-unused-vars
  for (let key in obj) {
    let contentItem = {};
    let objs = obj;

    contentItem[`${key}`] = objs[key];
    content.push(contentItem);
  }
  return content;
}
const Service5 = (props) => {
  const { id } = useParams();
  const [contents, setContents] = useState("");

  console.log("menuID", id);

  const ClickHandler = () => {
    window.scrollTo(10, 0);
  };

  //for get menu
  useEffect(() => {
    fetch(`${GET_CONTENTS_BY_MENU_ID}/${id || props.id}`)
      .then((response) => response.json())
      .then((data) => {
        setContents(data.menu_contents);

        console.log("allmenucontent", [data.menu_contents]);
      })
      .catch(() => {});
  }, [id, props.id]);

  //for content items
  const newContents = items(contents);
  console.log("content", newContents);

  return (
    <section className="wpo-features-section-s6 section-padding">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="wpo-section-title">
              <span>What We Do</span>
              <h2>Our Featured Services</h2>
              <p>
                There are many variations of passages of Lorem Ipsum available,
                but the majority have suffered alteration in some form,
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          {newContents.map((service, index) =>
            Object.entries(service).map(([key, value]) =>
              key === "Hotel Booking" ? (
                <div
                  className="col col-xl-4 col-lg-4 col-sm-6 col-12"
                  key={index}
                >
                  <div className="wpo-features-item">
                    <div className="wpo-features-icon">
                      <div className="icon">
                        <i className={`fi  ${service?.fIcon1}`}></i>
                      </div>
                    </div>
                    <div>
                      <h2>
                        <Link
                          onClick={ClickHandler}
                          to={`/service-single/${index}`}
                        >
                          {key}
                        </Link>
                      </h2>
                      <Interweave
                        allowAttributes
                        allowElements
                        disableLineBreaks={true}
                        content={value}
                      />
                    </div>
                  </div>
                </div>
              ) : " " && key === "Hajj and Umrah" ? (
                <div
                  className="col col-xl-4 col-lg-4 col-sm-6 col-12"
                  key={index}
                >
                  <div className="wpo-features-item">
                    <div className="wpo-features-icon">
                      <div className="icon">
                        <i className={`fi  ${service?.fIcon1}`}></i>
                      </div>
                    </div>
                    <div>
                      <h2>
                        <Link
                          onClick={ClickHandler}
                          to={`/service-single/${index}`}
                        >
                          {key}
                        </Link>
                      </h2>
                      <Interweave
                        allowAttributes
                        allowElements
                        disableLineBreaks={true}
                        content={value}
                      />
                    </div>
                  </div>
                </div>
              ) : " " && key === "Air Ticketing" ? (
                <div
                  className="col col-xl-4 col-lg-4 col-sm-6 col-12"
                  key={index}
                >
                  <div className="wpo-features-item">
                    <div className="wpo-features-icon">
                      <div className="icon">
                        <i className={`fi  ${service?.fIcon1}`}></i>
                      </div>
                    </div>
                    <div>
                      <h2>
                        <Link
                          onClick={ClickHandler}
                          to={`/service-single/${index}`}
                        >
                          {key}
                        </Link>
                      </h2>
                      <Interweave
                        allowAttributes
                        allowElements
                        disableLineBreaks={true}
                        content={value}
                      />
                    </div>
                  </div>
                </div>
              ) : (
                " "
              )
            )
          )}
        </div>
      </div>
    </section>
  );
};

export default Service5;
